#include <iostream>
#include <iomanip>

using namespace std;

#include "book_subscription.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"

void output(Product* subscription)
{
	cout << subscription->Price << " ";
	cout << subscription->Quantity << " ";
	cout << subscription->Category_product << " ";
	cout << subscription->Name_product << " ";
	cout << '\n';
}

int main()
{
	setlocale(LC_ALL, "Russian");
	cout << "������������ ������ �1. GIT\n";
	cout << "������� �10.  \n";
	cout << "�����: ����� ���������� \n";
	cout << "14Z\n\n";
	Product* subscriptions[MAX_FILE_ROWS_COUNT];
	int size;
	try
	{
		read("data.txt", subscriptions, size);
		for (int i = 0; i < size; i++)
		{
			cout << subscriptions[i]->Price << " ";
			cout << subscriptions[i]->Quantity << " ";
			cout << subscriptions[i]->Category_product << " ";
			cout << subscriptions[i]->Name_product << " ";
			cout << '\n';
		}
		bool (*check_function)(Product*) = NULL; 

		cout << "1) ������� ��� ������ � ��������� ������������\n";
		cout << "2) ������� ��� ������ ���������� ���� 100 ������\n";
		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = check_product_by_Name;      
			break;
		case 2:
			check_function = check_product_by_Price;      
			break;
		default:
			throw "������� ���������� �����";
		}
		if (check_function)
		{
			int new_size;
			Product** filtered = filter(subscriptions, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
		for (int i = 0; i < size; i++)
		{
			delete subscriptions[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
	return 0;
}
