#ifndef FILTER_H
#define FILTER_H

#include "book_subscription.h"

Product** filter(Product* array[], int size, bool (*check)(Product* element), int& result_size);

bool check_product_by_Name(Product* element);
bool check_product_by_Price(Product* element);

#endif
