#ifndef FILE_READER_H
#define FILE_READER_H

#include "book_subscription.h"

void read(const char* file_name, Product* array[], int& size);

#endif